<?php

namespace frontend\controllers\ajax;

use common\models\Player;
use Yii;
use yii\web\Controller;

class PlayerController extends Controller
{
    public function actionAddPlayer()
    {
        //var_dump(Yii::$app->request->post('name'));die;
        $player = new Player();
        if (!$player->load(Yii::$app->request->post('name')) && !$player->save()) {
            return false;
        }

        return true;

    }
}
