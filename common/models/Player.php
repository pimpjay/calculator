<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * Class Player
 * @package common\models
 * @property string $name
 */
class Player extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%player}}';
    }

    public function rules()
    {
        return [
            ['name', 'required'],
            ['name', 'string', 'min' => 2, 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'   => 'ID',
            'name' => 'Игрок',
        ];
    }
}
